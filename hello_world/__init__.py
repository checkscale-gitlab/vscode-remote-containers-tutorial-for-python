"""imports Flask Application
"""
from flask import Flask

# pylint: disable=trailing-whitespace


def create_app():
    """Flask application factory
    
        Returns:
        Flask-- Flask application object
    """
    app = Flask(__name__)
    return app
